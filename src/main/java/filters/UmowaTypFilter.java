package filters;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter({"/main"})
public class UmowaTypFilter implements Filter{

	FilterConfig filterConfig = null;

	public void init(FilterConfig config) throws ServletException {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
		FilterChain chain) throws IOException, ServletException {
		
		response.setContentType("text/html");
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		PrintWriter out = resp.getWriter();

		req.getSession().setAttribute("rodzaj", req.getParameter("rodzaj").toString());
		req.getSession().setAttribute("rok", req.getParameter("rok").toString());
		req.getSession().setAttribute("typ", req.getParameter("typ").toString());
		req.getSession().setAttribute("wynagr", req.getParameter("wynagr").toString());
		
		if (req.getParameter("rodzaj").equals("praca"))chain.doFilter(req, resp);
		else if (req.getParameter("rodzaj").equals("dzielo"))resp.sendRedirect("dzielo");
		else if (req.getParameter("rodzaj").equals("zlecenie"))resp.sendRedirect("zlecenie");
		out.close();
	}
	public void destroy() {}
}