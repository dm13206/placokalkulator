package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/zlecenie")
public class Zlecenie1Servlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		out.println("<html><body><h2>Dodatkowy formularz - umowa zlecenie</h2>"
				+"Forma umowy: "
				+request.getSession().getAttribute("contractKind")
				+ "<form action='zlecenie2'>" 
				
				+ "<fieldset id='skladkaRentowa'name='skladkaRentowa'>Skladka rentowa<br>"
				+ "<input type='radio' name='skladkaRentowa' value='tak' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaRentowa' value='nie'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='skladkaEmerytalna'name='skladkaEmerytalna'>Skladka emerytalna<br>"
				+ "<input type='radio' name='skladkaEmerytalna' value='tak' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaEmerytalna' value='nie'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='skladkaChorobowa'name='skladkaChorobowa'>Skladka chorobowa<br>"
				+ "<input type='radio' name='skladkaChorobowa' value='tak' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaChorobowa' value='nie'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='skladkaZdrowotna'name='skladkaZdrowotna'>Skladka zdrowotna<br>"
				+ "<input type='radio' name='skladkaZdrowotna' value='tak' checked='checked'/>tak<br>"
				+ "<input type='radio' name='skladkaZdrowotna' value='nie'/>nie<br>"
				+ "</fieldset>"
				+ "<fieldset id='kosztyUzyskaniaPrzychodu'name='kosztyUzyskaniaPrzychodu'>Koszty uzyskania przychodu<br>"
				+ "<input type='radio' name='kosztyUzyskaniaPrzychodu' value='20' checked='checked'/>20%<br>"
				+ "<input type='radio' name='kosztyUzyskaniaPrzychodu' value='50'/>50%<br>"
				+ "</fieldset>"
					+ "<input type='submit' value='dalej'/><br>"
				+ "</form>"
				
			+ "</body></html>");
		out.close();
	}
}