package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import count.DzieloBrutto;
import count.DzieloNetto;

@WebServlet("/dzielo2")
public class Dzielo2Servlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String kosztyUzyskaniaPrzychodu = request.getParameter("kosztyUzyskaniaPrzychodu");
		
		if(request.getSession().getAttribute("quotaType").equals("brutto")){
			
			DzieloBrutto count= new DzieloBrutto(Double.parseDouble((String) request.getSession().getAttribute("amount")),kosztyUzyskaniaPrzychodu);
			out.println("<html><body><h2>Oto wyniki!</h2>" +
					"Tabela dla umowy typu "+request.getSession().getAttribute("rodzaj")+"!<br>"+
					"dla roku "+request.getSession().getAttribute("rok")+"!<br>"+
					"<table border='1'>"+
					"<tr><td>Brutto</td><td>"+
					"Koszt uzyskania przychodu</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>Netto</td></tr>");
	
			out.println("<tr><td>" + count.getBrutto() +"</td><td>"+count.getKosztUzyskania()+"</td><td>"+count.getPodstawa()+"</td><td>"+count.getZalPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
		else{
			DzieloNetto count= new DzieloNetto(Double.parseDouble((String) request.getSession().getAttribute("amount")),kosztyUzyskaniaPrzychodu);
			out.println("<html><body><h2>Oto wyniki!</h2>" +
					"Tabela dla umowy typu "+request.getSession().getAttribute("rodzaj")+"!<br>"+
					"dla roku "+request.getSession().getAttribute("rok")+"!<br>"+
					"<table border='1'>"+
					"<tr><td>Brutto</td><td>"+
					"Koszt uzyskania przychodu</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>Netto</td></tr>");
	
			out.println("<tr><td>" + count.getBrutto() +"</td><td>"+count.getKosztUzyskania()+"</td><td>"+count.getPodstawa()+"</td><td>"+count.getZalPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
	}
}