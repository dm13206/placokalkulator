package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import count.PracaBrutto;
import count.PracaNetto;
import domain.Umowa;

@WebServlet("/praca")
public class PracaServlet extends HttpServlet {
private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = ((HttpServletRequest) request).getSession();
		
		Umowa umowa = new Umowa();
		umowa.setRodzaj(request.getParameter("rodzaj"));
		umowa.setRok(request.getParameter("rok"));
		umowa.setTyp(request.getParameter("typ"));
		umowa.setWynagr(request.getParameter("wynagr"));
		
		session.setAttribute("um",umowa);
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		if(request.getParameter("typ").equals("brutto")){
			PracaBrutto count=new PracaBrutto(Double.parseDouble(request.getParameter("wynagr")));
			out.println("<html><body>" +
					"Typ umowy: "+request.getParameter("rodzaj")+"<br>"+
					"Rok: "+request.getParameter("rok")+"<br>"+
					"<table border='1'>"+
					"<tr><td></td><td>Brutto</td><td>"+
					"emerytalne</td><td>rentowe</td><td>"+
					"chorobowe</td><td>zdrowotne</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>netto</td></tr>");
			for(int i=0;i<12;i++){
			out.println("<tr><td>"+ count.getMiesiac(i)+"</td><td>" + count.getBrutto() +"</td><td>"+count.getEmerytalne()+"</td><td>"+count.getRentowe()+"</td><td>"+count.getChorobowe()+"</td><td>"+count.getZdrowotne()+"</td><td>"+count.getPodstawa()+"</td><td>"+count.getZalPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			}
			out.println("<tr><td>Suma</td><td>" + 12*(count.getBrutto()) +
					"</td><td>"+12*(count.getEmerytalne())+
					"</td><td>"+12*(count.getRentowe())+
					"</td><td>"+12*(count.getChorobowe())+
					"</td><td>"+12*(count.getZdrowotne())+
					"</td><td>"+12*(count.getPodstawa())+
					"</td><td>"+12*(count.getZalPITdoZaplaty())+
					"</td><td>"+12*(count.getNetto())+
					"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
		else if(request.getParameter("typ").equals("netto")){
			PracaNetto count=new PracaNetto(Double.parseDouble(request.getParameter("wynagr")));
			out.println("<html><body>" +
					"Typ umowy: "+request.getParameter("rodzaj")+"<br>"+
					"Rok: "+request.getParameter("rok")+"<br>"+
					"<table border='1'>"+
					"<tr><td></td><td>Brutto</td><td>"+
					"emerytalne</td><td>rentowe</td><td>"+
					"chorobowe</td><td>zdrowotne</td><td>Podstawa opodatkowania</td><td>"+
					"Zaliczka na PIT</td><td>netto</td></tr>");
			for(int i=0;i<12;i++){
				out.println("<tr><td>"+ count.getMiesiac(i)+"</td><td>" + count.getBrutto() +"</td><td>"
						+count.getEmerytalne()+"</td><td>"+count.getRentowe()+"</td><td>"+count.getChorobowe()+"</td><td>"
						+count.getZdrowotne()+"</td><td>"+count.getPodstawa()+"</td><td>"
						+count.getZalPITdoZaplaty()+"</td><td>"+count.getNetto()+"</td></tr>");
			}
			out.println("<tr><td>Suma</td><td>" + 12*(count.getBrutto()) +
					"</td><td>"+12*(count.getEmerytalne())+
					"</td><td>"+12*(count.getRentowe())+
					"</td><td>"+12*(count.getChorobowe())+
					"</td><td>"+12*(count.getZdrowotne())+
					"</td><td>"+12*(count.getPodstawa())+
					"</td><td>"+12*(count.getZalPITdoZaplaty())+
					"</td><td>"+12*(count.getNetto())+
					"</td></tr>");
			out.println("</table>"+
				"</body></html>");
			out.close();
		}
	}

}
