package count;

public class PracaBrutto{

	private double brutto;
	private double emerytalne;
	private double rentowe;
	private double chorobowe;
	private double zdrowotne;
	private double podstawa;
	private double zalPIT;
	private double netto;
	private double poOdtraceniu;
	private double doOdliczenia;
	private double zalPITdoZaplaty;
	private String wynik;
	private String[]miesiac=new String[12];

	public PracaBrutto(double brutto) {
		this.brutto = brutto;
		emerytalne=brutto*(0.0976);
		rentowe=brutto*(0.015);
		chorobowe=brutto*(0.0245);
		zdrowotne=brutto*(0.0775);
		podstawa=brutto-(emerytalne+rentowe+chorobowe+zdrowotne);
		zalPIT=(brutto*(0.18))-46.33;
		poOdtraceniu=(brutto)-(emerytalne+rentowe+chorobowe);
		doOdliczenia=poOdtraceniu*0.0775;
		zalPITdoZaplaty=zalPIT-doOdliczenia;
		netto=podstawa-zalPITdoZaplaty;
		
		miesiac[0]="styczen";
		miesiac[1]="luty";
		miesiac[2]="marzec";
		miesiac[3]="kwiecien";
		miesiac[4]="maj";
		miesiac[5]="czerwiec";
		miesiac[6]="lipiec";
		miesiac[7]="sierpien";
		miesiac[8]="wrzesien";
		miesiac[9]="pazdziernik";
		miesiac[10]="listopad";
		miesiac[11]="grudzien";
	}
	public String getMiesiac(int i) {
		return miesiac[i];
	}
	public void setMiesiac(String[] months) {
		this.miesiac = months;
	}
	public double getBrutto() {
		return brutto;
	}
	public void setBrutto(double brutto) {
		this.brutto = brutto;
	}
	public double getEmerytalne() {
		return emerytalne;
	}
	public void setEmerytalne(double emerytalne) {
		this.emerytalne = emerytalne;
	}
	public double getRentowe() {
		return rentowe;
	}
	public void setRentowe(double rentowe) {
		this.rentowe = rentowe;
	}
	public double getChorobowe() {
		return chorobowe;
	}
	public void setChorobowe(double chorobowe) {
		this.chorobowe = chorobowe;
	}
	public double getZdrowotne() {
		return zdrowotne;
	}
	public void setZdrowotne(double zdrowotne) {
		this.zdrowotne = zdrowotne;
	}
	public double getPodstawa() {
		return podstawa;
	}
	public void setPodstawa(double podstawa) {
		this.podstawa = podstawa;
	}
	public double getZalPIT() {
		return zalPIT;
	}
	public void setZalPIT(double zalPIT) {
		this.zalPIT = zalPIT;
	}
	public double getNetto() {
		return netto;
	}
	public void setNetto(double netto) {
		this.netto = netto;
	}
	public String getWynik() {
		return wynik;
	}
	public void setWynik(String wynik) {
		this.wynik = wynik;
	}
	public double getPoOdtraceniu() {
		return poOdtraceniu;
	}
	public void setPoOdtraceniu(double poOdtraceniu) {
		this.poOdtraceniu = poOdtraceniu;
	}
	public double getDoOdliczenia() {
		return doOdliczenia;
	}
	public void setDoOdliczenia(double doOdliczenia) {
		this.doOdliczenia = doOdliczenia;
	}
	public double getZalPITdoZaplaty() {
		return zalPITdoZaplaty;
	}
	public void setZalPITdoZaplaty(double zalPITdoZaplaty) {
		this.zalPITdoZaplaty = zalPITdoZaplaty;
	}
}