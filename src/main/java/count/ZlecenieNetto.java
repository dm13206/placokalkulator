package count;

public class ZlecenieNetto {
	private double brutto;
	private double emerytalne;
	private double rentowe;
	private double chorobowe;
	private double zdrowotne;
	private double podstawa;
	private double zalPIT;
	private double netto;
	private double poOdtraceniu;
	private double doOdliczenia;
	private double zalPITdoZaplaty;
	private double kosztUzyskania;
	
	private String skladkaRentowa;
	private String skladkaEmerytalna;
	private String skladkaChorobowa;
	private String skladkaZdrowotna;
	private String kosztyUzyskaniaPrzychodu;
	
	public ZlecenieNetto(double netto,String skladkaRentowa,String skladkaEmerytalna,String skladkaChorobowa,String skladkaZdrowotna,String kosztyUzyskaniaPrzychodu) {
		this.netto = netto;
		brutto=netto*1.3741;
		this.skladkaRentowa = skladkaRentowa;
		this.skladkaEmerytalna = skladkaEmerytalna;
		this.skladkaChorobowa = skladkaChorobowa;
		this.skladkaZdrowotna = skladkaZdrowotna;
		this.kosztyUzyskaniaPrzychodu = kosztyUzyskaniaPrzychodu;
		
		if(skladkaRentowa.equals("nie"))rentowe=0;
		else if(skladkaRentowa.equals("tak"))rentowe=brutto*(0.015);
		
		if(skladkaEmerytalna.equals("nie"))emerytalne=0;
		else if(skladkaEmerytalna.equals("tak"))emerytalne=brutto*(0.0976);
		
		if(skladkaChorobowa.equals("nie"))chorobowe=0;
		else if(skladkaChorobowa.equals("tak"))chorobowe=brutto*(0.0245);
		
		if(skladkaZdrowotna.equals("nie"))zdrowotne=0;
		else if(skladkaZdrowotna.equals("tak"))zdrowotne=brutto*(0.0775);
		
		if(kosztyUzyskaniaPrzychodu.equals("20"))kosztUzyskania=(brutto-(chorobowe+emerytalne))*(0.2);
		else if(kosztyUzyskaniaPrzychodu.equals("50"))kosztUzyskania=(brutto-(chorobowe+emerytalne))*(0.5);
		
		podstawa=(brutto-(chorobowe+emerytalne))-kosztUzyskania;
		zalPIT=((brutto-(chorobowe+emerytalne))-kosztUzyskania)*0.18;

		zalPITdoZaplaty=(((brutto-(chorobowe+emerytalne))-kosztUzyskania)*0.18)-(brutto*0.0775);		
	}
	
	
	
	
	public double getBrutto() {
		return brutto;
	}
	public void setBrutto(double brutto) {
		this.brutto = brutto;
	}
	public double getEmerytalne() {
		return emerytalne;
	}
	public void setEmerytalna(double emerytalne) {
		this.emerytalne = emerytalne;
	}
	public double getRentowe() {
		return rentowe;
	}
	public void setRentowe(double rentowe) {
		this.rentowe = rentowe;
	}
	public double getChorobowe() {
		return chorobowe;
	}
	public void setChorobowe(double chorobowe) {
		this.chorobowe = chorobowe;
	}
	public double getZdrowotne() {
		return zdrowotne;
	}
	public void setZdrowotne(double zdrowotne) {
		this.zdrowotne = zdrowotne;
	}
	public double getPodstawa() {
		return podstawa;
	}
	public void setPodstawa(double podstawa) {
		this.podstawa = podstawa;
	}
	public double getZalPIT() {
		return zalPIT;
	}
	public void setZalPIT(double zalPIT) {
		this.zalPIT = zalPIT;
	}
	public double getNetto() {
		return netto;
	}
	public void setNetto(double netto) {
		this.netto = netto;
	}
	public double getPoOdtraceniu() {
		return poOdtraceniu;
	}
	public void setPoOdtraceniu(double poOdtraceniu) {
		this.poOdtraceniu = poOdtraceniu;
	}
	public double getDoOdliczenia() {
		return doOdliczenia;
	}
	public void setDoOdliczenia(double doOdliczenia) {
		this.doOdliczenia = doOdliczenia;
	}
	public double getZalPITdoZaplaty() {
		return zalPITdoZaplaty;
	}
	public void setZalPITdoZaplaty(double zalPITdoZaplaty) {
		this.zalPITdoZaplaty = zalPITdoZaplaty;
	}
	public double getKosztUzyskania() {
		return kosztUzyskania;
	}
	public void setKosztUzPrzy(double kosztUzyskania) {
		this.kosztUzyskania = kosztUzyskania;
	}
}
