package domain;

public class Umowa {

	private String rodzaj;
	private String rok;
	private String typ;
	private String wynagr;
	
	public String getRodzaj() {
		return rodzaj;
	}
	public void setRodzaj(String umowa) {
		this.rodzaj = umowa;
	}
	public String getRok() {
		return rok;
	}
	public void setRok(String rok) {
		this.rok = rok;
	}
	public String getTyp() {
		return typ;
	}
	public void setTyp(String typ) {
		this.typ = typ;
	}
	public String getWynagr() {
		return wynagr;
	}
	public void setWynagr(String wynagr) {
		this.wynagr = wynagr;
	}
}